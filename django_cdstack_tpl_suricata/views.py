import collections
import copy

import yaml
from django.template import engines

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
)
from django_cdstack_tpl_deb_buster.django_cdstack_tpl_deb_buster.views import (
    handle as handle_deb_buster,
)
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
)
from django_cdstack_tpl_crowdsec_main.django_cdstack_tpl_crowdsec_main.views import (
    handle as handle_crowdsec_main,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)


def dict_merge(dct, merge_dct):
    """Recursive dict merge. Inspired by :meth:``dict.update()``, instead of
    updating only top-level keys, dict_merge recurses down into dicts nested
    to an arbitrary depth, updating keys. The ``merge_dct`` is merged into
    ``dct``.
    :param dct: dict onto which the merge is executed
    :param merge_dct: dct merged into dct
    :return: None
    """
    for k, v in merge_dct.items():
        if (
            k in dct
            and isinstance(dct[k], dict)
            and isinstance(merge_dct[k], collections.Mapping)
        ):
            dict_merge(dct[k], merge_dct[k])
        else:
            dct[k] = merge_dct[k]


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_suricata/django_cdstack_tpl_suricata"

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    # load template
    suricata_conf = yaml.safe_load(
        open(
            module_prefix + "/templates/config-fs/dynamic/etc/suricata/suricata.yaml",
            "r",
        )
    )

    suricata_conf_custom = {
        "reputation-categories-file": "/etc/suricata/rules/scirius-categories.txt",
        "default-reputation-path": "/etc/suricata/rules",
        "reputation-files": ["scirius-iprep.list"],
        "default-rule-path": "/etc/suricata/rules",
        "rule-files": ["scirius.rules"],
        "classification-file": "/etc/suricata/rules/classification.config",
        "reference-config-file": "/etc/suricata/rules/reference.config",
        "threshold-file": "/etc/suricata/rules/threshold.config",
        "daemon-directory": "/var/log/suricata/core",
        "detect": {
            "profile": "high",
            "custom-values": {"toclient-groups": 3, "toserver-groups": 25},
            "sgh-mpm-context": "auto",
            "inspection-recursion-limit": 3000,
            "prefilter": {"default": "auto"},
            "grouping": {
                "tcp-whitelist": "53, 80, 139, 443, 445, 1433, 3306, 3389, 6666, 6667, 8080",
                "udp-whitelist": "53, 135, 5060",
            },
        },
        "runmode": "workers",
        "default-log-dir": "/var/log/suricata/",
        "stats": {
            "enabled": "yes",
            "interval": 8,
            "decoder-events": True,
            "decoder-events-prefix": "decoder.event",
            "stream-events": True,
        },
        "outputs": [
            {"fast": {"enabled": "no", "filename": "fast.log", "append": "yes"}},
            {
                "eve-log": {
                    "enabled": "yes",
                    "filetype": "regular",
                    "filename": "eve.json",
                    "pcap-file": True,
                    "community-id": True,
                    "community-id-seed": 13,
                    "xff": {
                        "enabled": "yes",
                        "mode": "extra-data",
                        "deployment": "reverse",
                        "header": "X-Forwarded-For",
                    },
                    "types": [
                        {
                            "alert": {
                                "payload": "yes",
                                "payload-printable": "yes",
                                "packet": "yes",
                                "http-body": "yes",
                                "http-body-printable": "yes",
                                "tagged-packets": "yes",
                            }
                        },
                        {
                            "anomaly": {
                                "enabled": "yes",
                                "types": {
                                    "decode": "no",
                                    "stream": "no",
                                    "applayer": "yes",
                                },
                            }
                        },
                        "flow",
                        {
                            "http": {
                                "extended": "yes",
                                "custom": [
                                    "accept",
                                    "accept-charset",
                                    "accept-encoding",
                                    "accept-language",
                                    "accept-datetime",
                                    "authorization",
                                    "cache-control",
                                    "cookie",
                                    "from",
                                    "max-forwards",
                                    "origin",
                                    "pragma",
                                    "proxy-authorization",
                                    "range",
                                    "te",
                                    "via",
                                    "x-requested-with",
                                    "dnt",
                                    "x-forwarded-proto",
                                    "accept-range",
                                    "age",
                                    "allow",
                                    "connection",
                                    "content-encoding",
                                    "content-language",
                                    "content-length",
                                    "content-location",
                                    "content-md5",
                                    "content-range",
                                    "content-type",
                                    "date",
                                    "etags",
                                    "last-modified",
                                    "link",
                                    "location",
                                    "proxy-authenticate",
                                    "referrer",
                                    "refresh",
                                    "retry-after",
                                    "server",
                                    "set-cookie",
                                    "trailer",
                                    "transfer-encoding",
                                    "upgrade",
                                    "vary",
                                    "warning",
                                    "www-authenticate",
                                    "true-client-ip",
                                    "org-src-ip",
                                    "x-bluecoat-via",
                                ],
                                "dump-all-headers": "both",
                            }
                        },
                        {"dns": {"version": 2}},
                        {"tls": {"extended": "yes"}},
                        {
                            "files": {
                                "force-magic": "yes",
                                "force-hash": ["md5", "sha1", "sha256"],
                            }
                        },
                        {
                            "smtp": {
                                "custom": [
                                    "received",
                                    "x-mailer",
                                    "x-originating-ip",
                                    "relays",
                                    "reply-to",
                                    "bcc",
                                    "reply-to",
                                    "bcc",
                                    "message-id",
                                    "subject",
                                    "x-mailer",
                                    "user-agent",
                                    "received",
                                    "x-originating-ip",
                                    "in-reply-to",
                                    "references",
                                    "importance",
                                    "priority",
                                    "sensitivity",
                                    "organization",
                                    "content-md5",
                                    "date",
                                ],
                                "md5": ["body", "subject"],
                            }
                        },
                        "dnp3",
                        "ftp",
                        "rdp",
                        "nfs",
                        "smb",
                        "tftp",
                        "ikev2",
                        "dcerpc",
                        "krb5",
                        "snmp",
                        "rfb",
                        "sip",
                        "ssh",
                        {"mqtt": None},
                        {"dhcp": {"enabled": "yes", "extended": "yes"}},
                        "http2",
                        {"stats": {"totals": "yes", "threads": "no", "deltas": "yes"}},
                        "flow",
                        "metadata",
                    ],
                }
            },
            {
                "unified2-alert": {
                    "enabled": "no",
                    "filename": "unified2.alert",
                    "xff": {
                        "enabled": "no",
                        "mode": "extra-data",
                        "deployment": "reverse",
                        "header": "X-Forwarded-For",
                    },
                }
            },
            {"http-log": {"enabled": "no", "filename": "http.log", "append": "yes"}},
            {"tls-log": {"enabled": "no", "filename": "tls.log", "append": "yes"}},
            {"tls-store": {"enabled": "no"}},
            {"dns-log": {"enabled": "no", "filename": "dns.log", "append": "yes"}},
            {
                "pcap-log": {
                    "enabled": "no",
                    "filename": "log.%n.%t.pcap",
                    "limit": "10mb",
                    "max-files": 20,
                    "mode": "multi",
                    "dir": "/data/nsm/",
                    "use-stream-depth": "no",
                    "honor-pass-rules": "no",
                }
            },
            {
                "alert-debug": {
                    "enabled": "no",
                    "filename": "alert-debug.log",
                    "append": "yes",
                }
            },
            {
                "alert-prelude": {
                    "enabled": "no",
                    "profile": "suricata",
                    "log-packet-content": "no",
                    "log-packet-header": "yes",
                }
            },
            {
                "stats": {
                    "enabled": "yes",
                    "filename": "stats.log",
                    "totals": "yes",
                    "threads": "no",
                }
            },
            {"syslog": {"enabled": "no", "facility": "local5"}},
            {"drop": {"enabled": "no", "filename": "drop.log", "append": "yes"}},
            {"file-store": {"version": 2, "enabled": "no"}},
            {
                "file-store": {
                    "enabled": "no",
                    "log-dir": "files",
                    "force-magic": "no",
                    "force-filestore": "no",
                    "include-pid": "no",
                }
            },
            {
                "file-log": {
                    "enabled": "no",
                    "filename": "files-json.log",
                    "append": "yes",
                    "force-magic": "yes",
                    "force-hash": ["md5", "sha1", "sha256"],
                }
            },
            {"tcp-data": {"enabled": "no", "type": "file", "filename": "tcp-data.log"}},
            {
                "http-body-data": {
                    "enabled": "no",
                    "type": "file",
                    "filename": "http-data.log",
                }
            },
            {"lua": {"enabled": "no", "scripts": None}},
        ],
        "logging": {
            "default-log-level": "notice",
            "default-output-filter": None,
            "outputs": [
                {"console": {"enabled": "yes"}},
                {
                    "file": {
                        "enabled": "yes",
                        "level": "info",
                        "filename": "/var/log/suricata/suricata.log",
                    }
                },
                {
                    "syslog": {
                        "enabled": "no",
                        "facility": "local5",
                        "format": "[%i] <%d> -- ",
                    }
                },
            ],
        },
        "app-layer": {
            "protocols": {
                "rfb": {
                    "enabled": "yes",
                    "detection-ports": {
                        "dp": "5900, 5901, 5902, 5903, 5904, 5905, 5906, 5907, 5908, 5909"
                    },
                },
                "mqtt": {"enabled": "yes", "max-msg-length": "1mb"},
                "krb5": {"enabled": "yes"},
                "snmp": {"enabled": "yes"},
                "ikev2": {"enabled": "yes"},
                "tls": {
                    "enabled": "yes",
                    "detection-ports": {"dp": 443},
                    "ja3-fingerprints": "yes",
                },
                "dcerpc": {"enabled": "yes"},
                "ftp": {"enabled": "yes"},
                "rdp": {"enabled": "yes"},
                "ssh": {"enabled": "yes"},
                "http2": {"enabled": "yes"},
                "smtp": {
                    "enabled": "yes",
                    "mime": {
                        "decode-mime": "yes",
                        "decode-base64": "yes",
                        "decode-quoted-printable": "yes",
                        "header-value-depth": 2000,
                        "extract-urls": "yes",
                        "body-md5": "no",
                    },
                    "inspected-tracker": {
                        "content-limit": 100000,
                        "content-inspect-min-size": 32768,
                        "content-inspect-window": 4096,
                    },
                },
                "imap": {"enabled": "detection-only"},
                "msn": {"enabled": "detection-only"},
                "smb": {"enabled": "yes", "detection-ports": {"dp": "139, 445"}},
                "nfs": {"enabled": "yes"},
                "tftp": {"enabled": "yes"},
                "sip": {"enabled": "yes"},
                "dhcp": {"enabled": "yes"},
                "dns": {
                    "tcp": {"enabled": "yes", "detection-ports": {"dp": 53}},
                    "udp": {"enabled": "yes", "detection-ports": {"dp": 53}},
                },
                "http": {
                    "enabled": "yes",
                    "libhtp": {
                        "default-config": {
                            "personality": "IDS",
                            "request-body-limit": "2mb",
                            "response-body-limit": "2mb",
                            "request-body-minimal-inspect-size": "32kb",
                            "request-body-inspect-window": "4kb",
                            "response-body-minimal-inspect-size": "40kb",
                            "response-body-inspect-window": "16kb",
                            "response-body-decompress-layer-limit": 2,
                            "http-body-inline": "auto",
                            "swf-decompression": {
                                "enabled": "yes",
                                "type": "both",
                                "compress-depth": 0,
                                "decompress-depth": 0,
                            },
                            "double-decode-path": "no",
                            "double-decode-query": "no",
                        },
                        "server-config": None,
                    },
                },
                "modbus": {
                    "enabled": "yes",
                    "detection-ports": {"dp": 502},
                    "stream-depth": 0,
                },
                "dnp3": {"enabled": "yes", "detection-ports": {"dp": 20000}},
                "enip": {
                    "enabled": "yes",
                    "detection-ports": {"dp": 44818, "sp": 44818},
                },
                "ntp": {"enabled": "yes"},
            }
        },
        "asn1-max-frames": 256,
    }

    dict_merge(suricata_conf, suricata_conf_custom)

    if "ids_home_networks" not in template_opts:
        template_opts["ids_home_networks"] = "192.168.0.0/16,10.0.0.0/8,172.16.0.0/12"

    suricata_conf["vars"]["address-groups"]["HOME_NET"] = (
        "[" + template_opts["ids_home_networks"] + "]"
    )

    # suricata_conf['reputation-categories-file'] = '/etc/suricata/rules/scirius-categories.txt'
    #
    # suricata_conf['default-reputation-path'] = '/etc/suricata/rules'
    #
    # suricata_conf['reputation-files'] = [
    #     'scirius-iprep.list',
    # ]
    #
    # suricata_conf['default-rule-path'] = '/etc/suricata/rules'
    #
    # suricata_conf['rule-files'] = [
    #     'scirius.rules',
    # ]
    #
    # suricata_conf['classification-file'] = '/etc/suricata/rules/classification.config'
    #
    # suricata_conf['reference-config-file'] = '/etc/suricata/rules/reference.config'
    #
    # suricata_conf['threshold-file'] = '/etc/suricata/rules/threshold.config'
    #
    # suricata_conf['detect']['profile'] = 'high'
    #
    # suricata_conf['detect']['prefilter']['default'] = 'auto'
    #
    # suricata_conf['detect']['grouping']['tcp-whitelist'] = '53, 80, 139, 443, 445, 1433, 3306, 3389, 6666, 6667, 8080'
    # suricata_conf['detect']['grouping']['udp-whitelist'] = '53, 135, 5060'
    #
    # suricata_conf['runmode'] = 'workers'
    #
    # suricata_conf['default-log-dir'] = '/var/log/suricata/'
    #
    # suricata_conf['stats']['enabled'] = 'yes'
    # suricata_conf['stats']['interval'] = 8
    # suricata_conf['stats']['decoder-events'] = 'true'
    # suricata_conf['stats']['decoder-events-prefix'] = 'decoder.event'
    # suricata_conf['stats']['stream-events'] = 'true'

    zip_add_file(
        zipfile_handler,
        "/etc/suricata/suricata.yaml",
        yaml.dump(suricata_conf, default_flow_style=False),
    )

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_ipcop(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_fail2ban(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_crowdsec_main(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_wazuh_agent(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    if not skip_handle_os:
        handle_deb(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
